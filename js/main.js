

jQuery(document).ready(function($) {
	'use strict';
	
	//	Remove loader on page load
	$('#page-loader').fadeOut( 'slow', function(){			
		//	Scroll to the top of the page
		// window.scrollTo(0, 0);
	});

	$('.header .navbar .nav-link').bind('click', function( ev ) {
        var el = $(this);
        if( el.attr('href') ) {
	        $('html, body').stop().animate({
				scrollTop: $(el.attr('href')).offset().top - 85
	        }, 1500 ), ev.preventDefault();
	    }
    });

	var navMain = $('.navbar-collapse');
	navMain.on('click', 'a:not([data-toggle])', null, function () {
		navMain.collapse('hide');
	});

    $('#mobileScreens').on('slide.bs.carousel', function ( ev ) {
		$( '#download .list-group .list-group-item' ).removeClass( 'active' ).eq(ev.to).addClass( 'active' );

	});

    // var owls = { 'clients' : { 'data' : [], 'owl' : null }, 'corporates' : { 'data' : [], 'owl' : null } };
    var owls = { 'clients' : { 'data' : [], 'owl' : null, responsive:{ 0:{items:2},600:{items:4},1000:{items:4} } }, 'testimonials' : { 'data' : [], 'owl' : null, responsive:{ 0:{items:1},600:{items:2},1000:{items:3} } } };


    /*owls.clients.data = [
        {
          'img' : 'uploads/cns.png',
          'alt' : 'CNS COMNET'
        },
        {
          'img' : 'uploads/galgotias-university.png',
          'alt' : 'Galgotias University'
        },
        {
          'img' : 'uploads/jaipuria.png',
          'alt' : 'Jaipuria'
        },
        {
          'img' : 'uploads/mediatek.png',
          'alt' : 'Mediatek'
        },
        {
          'img' : 'uploads/spicejet.png',
          'alt' : 'Spicejet'
        },
    ];*/

    owls.clients.data = [
        {
          'img' : 'uploads/DLF.jpg',
          'alt' : 'DLF'
        },
        {
          'img' : 'uploads/m3m.jpg',
          'alt' : 'M3M'
        },
        {
          'img' : 'uploads/navodaya.png',
          'alt' : 'Navodaya'
        },
        {
          'img' : 'uploads/orchid.jpg',
          'alt' : 'Orchid'
        },
        {
          'img' : 'uploads/plam-grove.png',
          'alt' : 'Plam Grove'
        },
        {
          'img' : 'uploads/privvy.jpg',
          'alt' : 'Privvy'
        },
        {
          'img' : 'uploads/time.jpg',
          'alt' : 'Time'
        },
        {
          'img' : 'uploads/tulip-orange.jpg',
          'alt' : 'Tulip Orange'
        },
        {
          'img' : 'uploads/unitech.jpg',
          'alt' : 'Unitech'
        },
        {
          'img' : 'uploads/uptown.jpg',
          'alt' : 'Uptown'
        },
        {
          'img' : 'uploads/vatika.png',
          'alt' : 'Vatika'
        },
    ];

    owls.testimonials.data = [
        {
            'name' : 'Ajay Kakra',
            'body' : 'I found Gobbly quite convenient and easy to use. The items offered are really fresh and at par with the competition. The other thing that\'d like to point out is that these guys are very particular about hygiene. My overall experience is quite good.',
            'img'  : 'uploads/ajay-kakra.png',
            'alt'  : 'Ajay Kakra'
        },
        {
            'name' : 'Avyukt Aggarwal',
            'body' : 'I always wondered if there was a way to eliminate the human labour involved in retail service while maintaining the same customer experience. With Gobbly, the customer experience is even better. Physically picking up the items you want and watching them get billed automatically shows us how far the human race has come in terms of ideas & technology, and great companies like Gobbly are the ones who drive such growth. It\'s smooth, it makes lives easier, it\'s managed properly and not to mention, it\'s extremely cool. Gobbly for the win!',
            'img'  : 'uploads/avyukt-aggarwal.jpg',
            'alt'  : 'Avyukt Aggarwal'
        },
        {
            'name' : 'G.D. Narang',
            'body' : 'It is very nice of Gobbly to have chosen our society Palm Grove Heights for services through on the spot delivery of daily essentials. I have had a very positive experience so far. Whenever I reported any small issue for help, help was offered to resolve the issue.',
            'img'  : 'uploads/gd-narang.png',
            'alt'  : 'G.D. Narang'
        },
        {
            'name' : 'Manashi Baruah',
            'body' : 'I am really a satisfied Gobbly customer. Life is easier now when it comes to daily need items. Even at 2 am I can get the basic essential without any hassle. Also, backend support is fabulous. Issues, if any, gets resolved in no time. Thanks to the team.',
            'img'  : 'uploads/manashi-baruah.jpeg',
            'alt'  : 'Manashi Baruah'
        },
        {
            'name' : 'Poonam Sood',
            'body' : 'Gobbly has made my life SIGNIFICANTLY easier. As a working mom, I struggled a lot in finding quality products at odd hours. Being just a 1-minute walk away from my home, Gobbly really changed the way I shop for my daily needs. May it be fresh fruits & veggies, milk, curd, cheese or even some snacks for the kids, everything is available so conveniently just a few steps away from my doorstep! And that too around the clock! Gobbly has undoubtedly replaced the local kirana stores for me.',
            'img'  : 'uploads/poonam-sood.png',
            'alt'  : 'Poonam Sood'
        },
        {
            'name' : 'RK Mishra',
            'body' : 'Gobbly has brought convenience to our Iives. Since it is available 24x7, we can buy the essentials even at odd hours of need. I hope this continues.',
            'img'  : 'uploads/rk-mishra.png',
            'alt'  : 'RK Mishra'
        },
        {
            'name' : 'Rohit Agarwal',
            'body' : 'This is a really good concept, especially in the time of COVID when stepping out is a risky job. Gobbly has brought convenience in your own society where you just have to step down to shop for essentials',
            'img'  : 'uploads/rohit-agarwal.jpeg',
            'alt'  : 'Rohit Agarwal'
        },
        {
            'name' : 'Saurabhh Singhania',
            'body' : 'Gobbly has been a saviour when it comes to last-minute getting an ingredient or fulfilling that hunger pan in the middle of the night. It has made my life easier and the ease of using it makes me a happy customer. Would recommend Gobbly each day every day to everyone.',
            'img'  : 'uploads/saurabhh-singhania.png',
            'alt'  : 'Saurabhh Singhania'
        },
        {
            'name' : 'Smita Singh',
            'body' : 'Gobbly has become an extension of my fridge I have stopped stocking up my fridge in order to avail fresh products every day These guys are redefining convenience for me Totally love the fact that I can buy cheese for my midnight snacking even or 12 AM',
            'img'  : 'uploads/smita-singh.png',
            'alt'  : 'Smita Singh'
        },
        {
            'name' : 'Shrichand',
            'body' : 'I\'ve been using Gobbly for a month now and I find it extremely easy to use. The interface is user friendly and all I need to do is take a lift to shop for essentials.',
            'img'  : 'uploads/srichand.png',
            'alt'  : 'Shrichand'
        },
        {
            'name' : 'Vanya Sharma',
            'body' : 'I came across Gobbly when I was with a friend and was really craving some munchies. It was a little late and no nearby shop was open. We were walking within my society and saw a white, glowing light coming from three refrigerators placed in a park (Gobbly was new in my society at that time and this is the first time I was noticing it). I was enthralled. We bought Nachos, Coke & Maggi. I think people tend to overlook that apart from being fast, easy to use, contactless & available 24/7, Gobbly has got a \'Wow factor\', it instantly captures your attention. My experience as a customer has been absolutely enchanting. Definitely the future of retail."',
            'img'  : 'uploads/vanya-sharma.jpg',
            'alt'  : 'Vanya Sharma'
        },
        {
            'name' : 'Yuvraj Bhown',
            'body' : 'Absolutely love the experience, it\'s wow! Never imagined that buying your daily needs can be such an experience. It took less than 10 seconds to shop essentials for the day.',
            'img'  : 'uploads/yuvraj-bhown.png',
            'alt'  : 'Yuvraj Bhown'
        },
        {
            'name' : 'Rohan Khera',
            'body' : 'Gobbly- The Robo Grocer\' gels well with our hasty lifestyles. #nottoworry about cooking and unplanned meals anymore, just \'tap to scan\' & our home is anytime guests ready now. The machine is very much kids friendly, set them free to shop. We wish Gobbly to turn out to be the great \'Robo grocer\' of the times. Good luck.',
            'img'  : 'media/default-male.png',
            'alt'  : 'Rohan Khera'
        },
        {
            'name' : 'Suman Kapoor',
            'body' : 'Gobbly has become our go-to option during the pandemic given it is a contactless store. We\'re able to check the availability of a desired product on the app in order to restrict the trip downstairs. Thank you so much Gobbly!',
            'img'  : 'media/default-female.png',
            'alt'  : 'Suman Kapoor'
        },
    ];

    Object.keys( owls ).forEach ( function( name, idx ) {
		owls[ name ].owl = $( '#' + name + ' .owl-carousel' ).owlCarousel({
		    loop:false,
		    smartSpeed: 100,
		    autoplay: true,
		    autoplaySpeed: 1000000,
		    mouseDrag: true,
		    margin:70,
		    animateIn: 'slideInUp',
		    animateOut: 'fadeOut',
		    nav:false,
		    responsive : owls[ name ].responsive
		});

		$.each( owls[ name ].data, function(k, client) {
            var row = '';
            if( name === 'testimonials' ) {
                row = '<div class="testimonial"><img src="' + client.img + '" class="img-fluid rounded-circle mb-4" alt="' + client.alt + '"><h5 class="text-primary">' + client.name + '</h5><em>' + client.body + '</em></div>';
            }else {
                row = '<div class="client"><img src="' + client.img + '" class="img-fluid" alt="' + client.alt + '"></div>';
            }
            owls[ name ].owl.trigger('add.owl.carousel', [ jQuery( row ) ] );
		});

		owls[ name ].owl.trigger('refresh.owl.carousel');
    });
});

$(document).on('click', function (e){
	var menu_id = '#mainNav';
    var menu_opened = $(menu_id).hasClass('show');  
    if(!$(e.target).closest(menu_id).length && !$(e.target).is(menu_id) && menu_opened === true){
		$(menu_id).collapse('toggle');
    }
});

//AOS.init();

var map;
function initMap() {
	map = new google.maps.Map( document.getElementById('map'), {
		center: new google.maps.LatLng( 28.5275198, 77.0688995 ),
		styles: [{
	    	stylers: [{
	      		saturation: -100
	    	}]
	  	}],
	  	zoom: 11,
	});

	var locations = [
		{
			name 		: 'Amity Noida',
			address 	: '',
			url 		: 'https://goo.gl/maps/UcvC2QNqvLEftaYn8',
			latitude 	: 28.5439106,
			longitude 	: 77.3311236,
		},
		{
			name 		: 'Gohive Gurgaon',
			address 	: '',
			url 		: 'https://goo.gl/maps/oF3HNavMS1rwc6xq5',
			latitude 	: 28.511375,
			longitude 	: 77.0889537,
		},
		{
			name 		: 'Unitech Cyber Park Gurgaon',
			address 	: '',
			url 		: 'https://goo.gl/maps/CEHkqMsPVyZWD5op6',
			latitude 	: 28.44363,
			longitude 	: 77.0556123,
		},
		{
			name 		: 'GALGOTIA UNIVERSITY',
			address 	: 'Plot No. 2, Yamuna Expy, Opposite, Buddha International Circuit, Sector 17A, Greater Noida, Uttar Pradesh 203201',			
			url 		: 'https://goo.gl/maps/Rx6iDwLZDXvf3CXE7',
			latitude 	: 28.3645672,
			longitude 	: 77.5376238,
		},
		{
			name 		: 'JAIPURIA',
			address 	: 'A-32, Opposite IBM, Block A, Industrial Area, Sector 62, Noida, Uttar Pradesh 201309',			
			url 		: 'https://goo.gl/maps/JTcmpeHFb45Qemoe7',
			latitude 	:  28.626942,
			longitude 	:  77.357998,
		},
		{
			name 		: 'WOMENS HOSTEL',
			address 	: 'C-56A-1, (Opp. D-park Gate, No.3, C Block, Phase 2, Industrial Area, Sector 62, Noida, Uttar Pradesh 20130',			
			url 		: 'https://goo.gl/maps/VRopkQ3Q9LAjQn5L7',
			latitude 	: 28.6154422,
			longitude 	: 77.3626238,
		},
		{
			name 		: 'BND HOSTEL',
			address 	: 'Knowledge Park III, Greater Noida, Uttar Pradesh 201310',			
			url 		: 'https://goo.gl/maps/BMAcrkJYMoovkPiM9',
			latitude 	: 28.4696922,
			longitude 	: 77.4934988,
		},
		{
			name 		: 'RPH RESIDENCY',
			address 	: 'Knowledge Park II, Greater Noida, Uttar Pradesh 201310',			
			url 		: 'https://goo.gl/maps/iAgmcsDsxznuseHP8',
			latitude 	: 28.4580672,
			longitude 	: 77.4939988,
		},
		{
			name 		: 'CNS COMNET',
			address 	: '433, Udyog Vihar Phase III, Gurugram, Haryana 122016',			
			url 		: 'https://goo.gl/maps/7JqQssovDH7Hvh8E7',
			latitude 	: 28.5103172,
			longitude 	: 77.0883738,
		},
		{
			name 		: 'SPICE JET',
			address 	: '321, Phase 4, Udyog Vihar, Sector 19, Gurugram, Haryana 122016',		
			url 		: 'https://goo.gl/maps/nhSkJ2P42jN5Ctn49',
			latitude 	: 28.4998172,
			longitude 	: 77.0794988,
		},
		{
			name 		: 'SPICE JET',
			address 	: '340, Phase 4, Udyog Vihar, Sector 19, Gurugram, Haryana 122016	G32J+8H Gurugram, Haryana',			
			url 		: 'https://goo.gl/maps/Kbprvv6NWqNS13nX9',
			latitude 	: 28.5008172,
			longitude 	: 77.0792488,
		},{
			name 		: 'SPICE JET',
			address 	: '104, Phase4 , Udyog Vihar, Sector 19, Gurugram, Haryana 122016	F3VJ+92 Gurugram, Haryana',			
			url 		: 'https://goo.gl/maps/33koiFEiDhBuvaJL9',
			latitude 	: 28.4934422,
			longitude 	: 77.0778738,
		},
		{
			name 		: 'MEDIATEK',
			address 	: '1A/1 SB Tower, Sector 16A, Noida, Uttar Pradesh 201301	H888+97 Sector 16A, Noida, Uttar Pradesh',			
			url 		: 'https://goo.gl/maps/nSgZFobeRcE5nFfJ7',
			latitude 	: 28.5659422,
			longitude 	: 77.3134988,
		},
		{
			name 		: 'EZ STAY',
			address 	: '25B, Phase 3, Knowledge Park 3, near IIMT college, Greater Noida, Uttar Pradesh 201308	FFHR+C8 Greater Noida, Uttar Pradesh',			
			latitude 	: 28.4785672,
			longitude 	: 77.4886238,
		}
	];

	var infoWindow = new google.maps.InfoWindow();

	for (var i = 0; i < locations.length; i++) {

		var marker = new google.maps.Marker({
			position : new google.maps.LatLng( locations[i].latitude, locations[i].longitude ),
			icon 	 : 'http://gobbly.in/media/icon-location.png',
			map 	 : map,
		});

        (function (marker, location ) {
            marker.addListener('click', function() {
				var location = this.details;
				var content = '<div class="location-info" style="max-width: 20rem;">';
				content += '<h3 class="name">' + location.name + '</h3>';
				content += '<address class="address">' + location.address + '</address>';
				content += '<p class="map"><a href="' + location.url + '" target="_blank">View on map<a></p>';
				content += '</div>';
				infoWindow.setContent( content );
	          	infoWindow.open(map, marker);
	        });
        })( marker, locations[i] );
	};
}
